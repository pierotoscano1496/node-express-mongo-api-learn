const express = require('express');
const router = express.Router();
const Persona = require('../models/persona');

router.get('/api/listPersonas', async function (req, res) {
    var listPersonas = await Persona.find();
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(listPersonas));
});

router.get('/api/getPersonaById/:id', async function (req, res) {
    var persona = await Persona.findById(req.params.id);
    res.setHeader('Content-Type', 'application/json');
    res.send(JSON.stringify(persona));
});

router.post('/api/addPersona', async function (req, res) {
    var persona = new Persona(req.body);
    await persona.save();
    res.setHeader('Content-Type', 'text/plain');
    res.send('Persona registrada correctamente');
});

router.post('/api/updatePersona/:id', async function (req, res) {
    await Persona.updateOne({ _id: req.params.id }, req.body);
    res.setHeader('Content-Type', 'text/plain');
    res.send('Persona actualizada correctamente');
});

router.get('/api/deletePersona/:id', async function (req, res) {
    await Persona.deleteOne({ _id: req.params.id });
    res.setHeader('Content-Type', 'text/plain');
    res.send('Persona eliminada correctamente');
});

module.exports = router;