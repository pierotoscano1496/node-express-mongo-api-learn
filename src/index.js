const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const personaRoutes = require('./routes/personaRoutes');
const PORT = process.env.PORT || 3000;
const mongoose = require('mongoose');

//mongoose
mongoose.connect('mongodb://localhost/mongo-learn', { useNewUrlParser: true })
    .then(function (db) {
        console.log('Conectado a la base de datos');
    })
    .catch(function (err) {
        console.log('Error de conexión');
    });

//middleware
//CORS
app.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    console.log(new Date().toString() + ': ' + req.originalUrl);
    next();
});
app.use(bodyParser.json());
app.use(express.urlencoded({ extended: false }));

//personaRoutes.js
app.use(personaRoutes);

//error 404
app.use(function (req, res, next) {
    res.render('400', { message: 'Error 400' });
});

//error 500 (throwing errors)
app.use(function (err, req, res, next) {
    console.error(err.stack);
    res.render('500', { message: 'Error 500' });
});

//listen port
app.listen(PORT, function () {
    console.log('Escuchando en puerto: ' + PORT);
});