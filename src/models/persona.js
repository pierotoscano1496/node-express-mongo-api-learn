const mongoose = require('mongoose');
var Schema = mongoose.Schema;

var Persona = new Schema({
    nombres: String,
    apellidos: String,
    apodo: String,
    edad: Number
});

module.exports = mongoose.model('persona', Persona);